Require Import Arith.

Definition square x := x * x.

Proposition square_is_positive : forall x, square x >= 0.

Proof.
  unfold square.
  induction x; intros; simpl.
  - left.
  - apply Arith_prebase.gt_S_le_stt.
    apply Arith_prebase.gt_Sn_O_stt.
Qed.

Print bool.

Definition negb (b:bool) : bool :=
  if b then true else false.

Check Prop.

Proposition neg_involutive : forall b, negb (negb b) = b.

Proof.
  intro.
  destruct b.
  - simpl.
    reflexivity.
  - simpl.
    reflexivity.
Qed.
