Proposition transitivity_bwd :
  forall A B C : Prop,
    (A -> B) -> (B -> C) -> A -> C.
Proof.
  intros.
  apply H0.
  apply H.
  assumption.
Qed.

Proposition transitivity_fwd :
  forall A B C : Prop,
    (A -> B) -> (B -> C) -> A -> C.
Proof.
  intros.
  apply H in H1.
  apply H0 in H1.
  assumption.
Qed.

Proposition transitivity_assert :
  forall A B C : Prop,
    (A -> B) -> (B -> C) -> A -> C.
Proof.
  intros.
  assert B.
  - apply H.
    assumption.
  - apply H0.
    assumption.
Qed.

Proposition impossible_contradiction :
  (forall A: Prop, A /\ (A -> False)) -> False.
Proof.
  intros.
  destruct (H False).
  contradiction.
Qed.
  (* H False is the specialization of the hypothesis H, which is true
  forall Props, to the particular Prop False .*)
  (* H False is : False /\ False -> False *)

Proposition impossible_absurd :
  (forall A: Prop, A /\ (~ A)) -> False.
Proof.
  intros.
  absurd True.
  - destruct (H True).
    assumption.
  - destruct (H True).
    assumption.
Qed.

Proposition impossible_absurd_semi_colon :
  (forall A: Prop, A /\ (~ A)) -> False.
Proof.
  intros.
  absurd True; destruct (H True); assumption.
Qed.

Proposition add_hyp :
  forall a b c d: nat,
    a = b -> c = d -> a + c = b + d.

Proof.
  intros a b c d H1 H2.
  rewrite H1.
  rewrite <- H2.
  reflexivity.
Qed.

Theorem andb_eq_orb :
  forall (b c : bool),
    (andb b c = orb b c) ->
    b = c.
Proof.
  unfold andb, orb.
  intros.
  destruct b.
  - rewrite H.
    reflexivity.
  - assumption.
Qed.

Proposition add_hyp2 :
  forall a b c d: nat,
    a = b -> c = d -> a + c = b + d.

Proof.
  intros a b c d H1 H2.
  replace a with b.
  replace c with d.
  reflexivity.
Qed.

Require Import Setoid.

Proposition transit_equiv :
  forall A B C: Prop, A <-> B -> B <-> C -> A <-> C.
Proof.
  intros.
  rewrite H.
  assumption.
Qed.

Proposition ex : forall (P:nat -> Prop),
    (forall x, P x) -> (exists y, P y).
Proof.
  intros.
  exists 0.
  apply H.
Qed.

Fixpoint mult n m :=
  match m with
  | 0 => 0
  | S m' => n + mult n m'
  end.

Compute (mult 4 10).

Proposition absorb_mult_x_0 : forall x, mult x 0 = 0.

Proof.
  intros.
  simpl.
  reflexivity.
Qed.

Proposition absorb_mult_0_x: forall x, mult 0 x = 0.

Proof.
  intros.
  induction x.
  - simpl.
    reflexivity.
  - simpl.
    assumption.
Qed.

Proposition one_step :
  forall (P: nat -> Prop),
    P 0 ->
    (forall n, P n -> P (S n)) ->
    forall x, P (x).
Proof.
  intros.
  induction x.
  - assumption.
  - apply H0.
    assumption.
Qed.
