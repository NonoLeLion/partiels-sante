Theorem triple_equiv :
  forall A B C: Prop,
    (A -> B) -> (B -> C) -> (C -> A)
    -> (A <-> B) /\ (B <-> C) /\ (C <-> A).
Proof.
  intros A B C H1 H2 H3.
  split.
  - split.
    * assumption.
    * intro.
      apply H3.
      apply H2.
      assumption.
  - split.
    -- split.
       * assumption.
       * intro.
         apply H1.
         apply H3.
         assumption.
    -- split.
       * assumption.
       * intro.
         apply H2.
         apply H1.
         assumption.
Qed.

Theorem triple_equiv_2 (A:Prop) (B:Prop) (C:Prop):
  (A -> B) -> (B -> C) -> (C -> A)
  -> (A <-> B) /\ (B <-> C) /\ (C <-> A).
Proof.
  intros H1 H2 H3.
  repeat split; try assumption.
  - intro.
    apply H3.
    apply H2.
    assumption.
  - intro.
    apply H1.
    apply H3.
    assumption.
  - intro.
    apply H2.
    apply H1.
    assumption.
Qed.

Lemma transit (P:Prop) (Q:Prop) (R:Prop):
  (P -> Q) -> (Q -> R) -> (P -> R).
Proof.
  admit.
Admitted.

Theorem triple_equiv_3 (A:Prop) (B:Prop) (C:Prop):
  (A -> B) -> (B -> C) -> (C -> A)
  -> (A <-> B) /\ (B <-> C) /\ (C <-> A).
Proof.
  intros H1 H2 H3.
  repeat split; try assumption.
  - apply transit with (Q:=C); assumption.
  - apply transit with (Q:=A); assumption.
  - apply transit with (Q:=B); assumption.
Qed.
