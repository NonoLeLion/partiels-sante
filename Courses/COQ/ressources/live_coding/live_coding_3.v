Require Import List.
Import ListNotations.

Fixpoint mem (n:nat) l : Prop :=
  match l with
  | (h::t) => n = h \/ (mem n t)
  | [] => False
  end.

Proposition concat_keep_mem_1 :
  forall x l1 l2,
    mem x l1 \/ mem x l2 -> mem x (l1 ++ l2).
Proof.
  intros.
  induction l1.
  - simpl.
    destruct H.
    * simpl in H.
      contradiction.
    * assumption.
  - simpl app.
    simpl mem.
    destruct H.
    * simpl in H.
      destruct H.
      ** left.
         assumption.
      ** right.
         apply IHl1.
         left.
         assumption.
    * right.
      apply IHl1.
      right.
      assumption.
Qed.

Proposition concat_keep_mem_2 :
  forall x l1 l2,
     mem x (l1 ++ l2) -> mem x l1 \/ mem x l2.
Proof.
  intros.
  induction l1; simpl.
  - right.
    simpl in H.
    assumption.
  - Search ((_ \/ _) \/ _).
    rewrite or_assoc.
    simpl in H.
    destruct H.
    * left; assumption.
    * right.
      apply IHl1.
      assumption.
Qed.

Proposition concat_keep_mem :
  forall x l1 l2,
    mem x l1 \/ mem x l2 <-> mem x (l1 ++ l2).
Proof.
  intros.
  split.
  - apply concat_keep_mem_1.
  - apply concat_keep_mem_2.
Qed.
