Proposition plus_n_0 :
  forall n: nat, n+0=n.
Proof.
  induction n.
  - reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Proposition double_is_plus:
  forall n : nat, n+n=2*n.
Proof.
  intros n. simpl. rewrite plus_n_0. reflexivity.
Qed.

Proposition add_succ_r :
  forall n m: nat, n + S m = S (n + m).
Proof.
  intros n m.
  induction n as [| n' IHn'].
  - simpl. reflexivity.
  - simpl. rewrite IHn'. reflexivity.
Qed.

Proposition add_succ_l :
  forall n m: nat, S n + m = S (n + m).
Proof.
  intros n m. simpl. reflexivity.
Qed.

Fixpoint even (n:nat) : Prop :=
  match n with
  | 0 => True
  | S 0 => False
  | S (S m) => even(m)
  end.

Proposition one_of_two_succ_is_even:
  forall n : nat, (even n) \/ (even (S n)).
Proof.
  intros n.
  induction n.
  - left. reflexivity.
  - destruct IHn; [right | left]; assumption.
Qed.

Proposition but_not_both :
  forall n : nat, even n -> not (even (S n)).
Proof.
  unfold not.
  intros n H.
  induction n.
  - intro. assumption.
  - intro. apply IHn; assumption.
Qed.

Proposition double_is_even :
  forall n : nat, even (n*2).
Proof.
  intros n.
  induction n.
  - reflexivity.
  - assumption.
Qed.

Proposition succ_double_is_odd :
  forall n : nat, ~(even (S (n*2))).
Proof.
  unfold not.
  intros n.
  apply but_not_both.  
  apply double_is_even.
Qed.

Proposition pair_induction :
  forall (P : nat -> Prop),
  P 0 -> P 1 -> (forall n, P n -> P (S n) -> P (S (S n))) ->
  forall x, P x.
Proof.
  intros Pn P0 P1 H1 n.
  assert (Pn n /\ Pn(S n)).
  - induction n.
    + split; assumption.
    + destruct IHn as [Pn_n Pn_Sn].
      * split; try apply H1; assumption.
  - destruct H as [Pn_n Pn_Sn]. assumption.
Qed.

Proposition even_sum:
  forall n m: nat, even n -> even m -> even(n+m).
Proof.
  intros n m H1 H2.
  induction n using pair_induction; simpl; try assumption.
  - contradiction.
  - apply IHn. assumption.
Qed.

Definition example_function (x : nat) :
 nat :=  x * 0.

Lemma example_function_satisfies_mystery :
  mystery example_function.
Proof.
  unfold mystery.
  exists 1.
  split.
  - unfold gt.
    Search ( _ <=  S _).
    apply le_n_S.
    Search ( 0 <= _).
    apply le_0_n.
  - unfold example_function.
    intros.
    Search (_ = _ * 0).
    rewrite <- mult_n_O with (n := x + 1).
    Search (_ = _ -> _ = _).
    apply eq_sym.
    apply mult_n_O with (n := x).
Qed.
