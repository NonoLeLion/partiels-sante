Theorem modus_ponens: 
  forall A B : Prop,
 (A -> B) -> A -> B.
Proof.
  intros A B H1 H2.
  apply H1.
  exact H2.
Qed.

Theorem trans_implication:
  forall A B C : Prop,
  (A -> B) -> (B -> C) -> (A -> C).
Proof.
  intros A B C H1 H2 H3.
  apply H2. apply H1.
  exact H3.
Qed.


Theorem modus_tolens:
  forall A B : Prop,
  (A -> B) -> ~B -> ~A.
Proof.
  unfold not.
  intros A B H1 H2 H3.
  apply H2. apply H1.
  exact H3.
Qed.

Theorem q1:
  forall A B : Prop,
  (( A \/ B) -> False) -> (A -> False).
Proof.
  intros A B H1 H2.
  apply H1.
  left.
  exact H2.
Qed.

Theorem q1_2:
  forall A B : Prop,
 (( A \/ B) -> False) -> (B -> False).
Proof.
  intros A B H1 H2.
  apply H1.
  right.
  exact H2.
Qed.

Theorem de_morgan_bool_1:
  forall a b : bool,
  negb (orb a b) = andb (negb a) (negb b).
Proof.
  intros [] []; simpl; reflexivity.
Qed.

Theorem de_morgan_bool_2 :
  forall a b:bool,
 (orb (negb a) (negb b)) = negb (andb a b).
Proof.
  intros [] []; simpl; reflexivity.
Qed.

Theorem de_morgan_1 :
  forall P Q, ~(P \/ Q) -> ~P /\ ~Q.
Proof.
  unfold not.
  intros P Q H1.
  split; intros; apply H1; [left | right]; assumption.
Qed.

Theorem de_morgan_2 :
  forall P Q,
  ~P /\ ~Q -> ~(P \/ Q).
Proof.
  unfold not.
  intros P Q [H1 H2].
  intros [HP | HQ].
  - apply H1. exact HP.
  - apply H2. exact HQ.
Qed.

Theorem de_morgan_2_v2 :
  forall P Q,
  ~P /\ ~Q -> ~(P \/ Q).
Proof.
  unfold not.
  intros P Q [H1 H2].
  intros [HP | HQ].
  - apply H1. exact HP.
  - apply H2. exact HQ.
Qed.

Theorem de_morgan_1_2 :
  forall P Q,
  ~P /\ ~Q <-> ~(P \/ Q).
Proof.
  unfold not.
  split.
  - apply de_morgan_2.
  - apply de_morgan_1.
Qed.

Require Import Classical.

Theorem de_morgan_classic :
  forall P Q,
  ~P \/ ~Q <-> ~(P /\ Q).
Proof.
  unfold not.
  split.
  - intros [H1 | H2].
    + intros [HP HQ]. apply H1. exact HP.
    + intros [HP HQ]. apply H2. exact HQ.
  - intros.
    destruct (classic P) as [HP | HnotP].
      * right. intro HQ. apply H. split; assumption.
      * left. intro HP. apply HnotP. exact HP.
Qed.

Theorem excluded_middle_irrefutable:
  forall P:Prop,
  ~~(P \/ ~P).
Proof.
  unfold not.
  intros H1 H2.
  apply H2. right. intro.
  apply H2. left. assumption.
Qed.


Theorem double_neg_implies_excluded :
  (forall P: Prop,
   ~~ P -> P) -> (forall P, (P \/ ~P)).
Proof.
  intros double_neg P.
  apply double_neg.
  intro not_P.
  apply not_P.
  right.
  intro p.
  apply not_P.
  left.
  exact p.
Qed.

Theorem excluded_implies_demorgan:
  (forall P : Prop, P \/ ~P) ->
  (forall P Q : Prop, ~(P /\ Q) -> ~P \/ ~Q).
Proof.
  unfold not.
  intros H1 P1 P2 H2.
  destruct (H1 P1) as [HP | HnotP].
  - right. intro p2. apply H2. split; assumption.
  - left. assumption.
Qed.

Theorem double_neg_implies_demorgan:
(forall P: Prop,
   ~~ P -> P) ->
(forall P Q : Prop, ~ (P /\ Q) -> ~P \/ ~Q).
Proof.
  intros double_neg P Q H.
  apply double_neg_implies_excluded with (P:=P) in double_neg.
  destruct double_neg as [HP | HnotP].
  - right. intro. apply H; split; assumption.
  - left. assumption.
Qed.