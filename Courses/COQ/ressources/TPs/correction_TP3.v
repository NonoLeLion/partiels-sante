Require Import List.
Import ListNotations.

(* Exercice 1 : Concaténation *)

Proposition list_assoc:
  forall (A : Type) (a b c : list A),
  (a ++ b) ++ c = a ++ (b ++ c).
Proof.
  intros A a b c. (* Introduction des variables *)
  induction a as [| x xs IHa]. (* Induction sur la liste a *)
  - simpl. (* Cas de base : a est la liste vide *)
    reflexivity.
  - simpl. (* Cas inductif : a = x :: xs *)
    rewrite IHa. (* Utilisation de l'hypothèse d'induction *)
    reflexivity.
Qed.

Proposition right_nil_concat:
  forall (A : Type) (l : list A),
  l ++ nil = l.
Proof.
  intros A l. (* Introduction des variables *)
  induction l as [| x xs IHl]. (* Induction sur la liste l *)
  - simpl. (* Cas de base : l est la liste vide *)
    reflexivity.
  - simpl. (* Cas inductif : l = x :: xs *)
    rewrite IHl. (* Utilisation de l'hypothèse d'induction *)
    reflexivity.
Qed.

Proposition left_nil_concat:
  forall (A : Type) (l : list A),
  nil ++ l = l.
Proof.
  intros A l. (* Introduction des variables *)
  induction l as [| x xs IHl]. (* Induction sur la liste l *)
  - simpl. (* Cas de base : l est la liste vide *)
    reflexivity.
  - simpl. (* Cas inductif : l = x :: xs *)
    (* Ici, la simplification directe suffit, pas besoin de l'hypothèse d'induction *)
    reflexivity.
Qed.

(* Exercice 1 : Tailles *)

Fixpoint length {A : Type} (l : list A) : nat :=
  match l with
  | [] => 0 (* Cas de base : la longueur de la liste vide est 0 *)
  | (_ :: t) => 1 + length t (* Cas récursif : ajoute 1 à la longueur de la queue de la liste *)
  end.

Proposition concat_length_sum:
  forall (A : Set) (xs ys : list A),
  length (xs ++ ys) = length xs + length ys.
Proof.
  intros A xs ys. (* Introduction des variables *)
  induction xs as [| x xs IHxs]; simpl. (* Induction sur la liste xs *)
  - reflexivity. (* Cas de base : xs est la liste vide, la propriété est trivialement vraie *)
  - rewrite IHxs. (* Cas inductif : utilise l'hypothèse d'induction *)
    reflexivity.
Qed.

Fixpoint rev {A : Type} (l : list A) : list A :=
  match l with
  | [] => [] (* Cas de base : la liste vide, la liste inversée est également vide *)
  | (h :: t) => (rev t) ++ [h] (* Cas récursif : inverse la queue et ajoute la tête à la fin *)
  end.

Goal rev [1;2;3;0] = [0;3;2;1].
Proof.
  simpl. (* Utilise la simplification pour évaluer la fonction rev *)
  reflexivity. (* La liste inversée est égale à la liste attendue *)
Qed.

Proposition rev_same_len:
  forall (A : Set) (l : list A),
  length (rev l) = length l.
Proof.
  intros A l. (* Introduction des variables *)
  induction l as [| x xs IHl]; simpl. (* Induction sur la liste l *)
  - reflexivity. (* Cas de base : la liste vide, la longueur de la liste inversée est 0 *)
  - simpl. (* Cas inductif : utilise l'hypothèse d'induction *)
    rewrite concat_length_sum. (* Utilise la propriété concat_length_sum *)
    simpl. (* Simplification supplémentaire *)
    rewrite IHl. (* Utilise l'hypothèse d'induction sur la queue de la liste *)
    Search (_ + 1 = S _). (* Cherche un lemme sur l'addition avec 1 *)
    apply PeanoNat.Nat.add_1_r. (* Applique le lemme trouvé *)
Qed.

Fixpoint nth {A : Set} (i : nat) (xs : list A) : option A :=
  match xs with
  | [] => None (* Cas de base : la liste est vide, renvoie None *)
  | h :: t => 
    match i with
    | 0 => Some h (* Cas : l'index est 0, renvoie Some de la tête de la liste *)
    | S j => nth j t (* Cas récursif : décrémente l'index et continue avec la queue de la liste *)
    end
  end.

Goal nth 2 [1;2;3;4] = Some 3.
Proof.
  simpl. (* Utilise la simplification pour évaluer la fonction nth *)
  reflexivity. (* Le résultat de la fonction est Some 3, ce qui est égal à Some 3 *)
Qed.

Proposition nth_len_app1:
  forall (A : Set) (l1 l2 : list A),
  nth (length l1) (l1 ++ l2) = @nth A 0 l2.
Proof.
  intros A l1 l2. (* Introduction des variables *)
  induction l2 as [| x xs IHl2]. (* Induction sur la liste l2 *)
  - simpl. (* Cas de base : l2 est la liste vide *)
    rewrite right_nil_concat. (* Utilise la propriété right_nil_concat *)
    induction l1 as [| y ys IHl1].
    * simpl. (* Cas particulier : l1 est la liste vide *)
      reflexivity.
    * simpl. (* Cas inductif : l1 = y :: ys *)
      assumption. (* Utilise l'hypothèse d'induction sur la liste l1 *)
  - simpl. (* Cas inductif : l2 = x :: xs *)
    induction l1 as [| y ys IHl1].
    * simpl. (* Cas particulier : l1 est la liste vide *)
      reflexivity.
    * simpl. (* Cas inductif : l1 = y :: ys *)
      apply IHl1. (* Utilise l'hypothèse d'induction sur la liste l1 *)
      assumption. (* Assure que les conditions de l'induction sont satisfaites *)
Qed.

Proposition nth_len_app2:
  forall (A : Set) (l1 l2 : list A) (i : nat),
  (i < length l1) -> nth i (l1 ++ l2) = nth i l1.
Proof.
  intros A l1 l2 i H. (* Introduction des variables *)
  generalize dependent i. (* Généraliser i pour inclure H dans l'hypothèse inductive *)
  induction l1 as [| x xs IHl1].
  - intros i H. (* Cas de base : l1 est la liste vide *)
    inversion H. (* Contradiction : i ne peut pas être < 0 *)
  - intros i H. (* Cas inductif : l1 = x :: xs *)
    simpl in H. (* Simplification de l'inégalité *)
    destruct i as [| j].
    -- (* Cas : i = 0 *)
       reflexivity.
    -- (* Cas : i = S j *)
       simpl.
       apply IHl1. (* Utilise l'hypothèse d'induction *)
       Search ( S _ <= S _). 
       apply PeanoNat.Nat.succ_le_mono.
       assumption. (* Assure que les conditions de l'induction sont satisfaites *)
Qed.

(* Exercice 3 : Contenu des listes et rev *)

Lemma rev_app_distr:
  forall (A : Set) (l1 l2 : list A),
  rev (l1 ++ l2) = rev l2 ++ rev l1.
Proof.
  intros A l1 l2. (* Introduction des variables *)
  induction l1 as [| x xs IHl1]. (* Induction sur la liste l1 *)
  - simpl. (* Cas de base : l1 est la liste vide *)
    Search (_ ++ []).
    rewrite app_nil_r. (* Utilise la propriété app_nil_r *)
    reflexivity.
  - simpl. (* Cas inductif : l1 = x :: xs *)
    rewrite IHl1. (* Utilise l'hypothèse d'induction *)
    Search ((_ ++ _) ++ _ = _ ++ _ ++ _).
    rewrite app_assoc_reverse. (* Utilise la propriété app_assoc_reverse *)
    reflexivity.
Qed.

Proposition rev_involutive:
  forall (A : Set) (l : list A),
  rev (rev l) = l.
Proof.
  intros A l. (* Introduction des variables *)
  induction l as [| x xs IHl]. (* Induction sur la liste l *)
  - reflexivity. (* Cas de base : l est la liste vide, la propriété est trivialement vraie *)
  - simpl. (* Cas inductif : l = x :: xs *)
    rewrite rev_app_distr. (* Utilise le lemme rev_app_distr *)
    rewrite IHl. (* Utilise l'hypothèse d'induction *)
    simpl. (* Simplification supplémentaire *)
    reflexivity.
Qed.

