(* The goal of this exam is to prove the equality:

a * b = b * a.

It is split into 7 intermediary propositions you will have to prove.

Your goal is to replace the "admit. Admitted." part by an actual
proof, validated by Qed.

you MUST respect the following instructions

- only tactics from the cheatsheet are authorized
- lemmas from the standard library are forbidden.
- do not change the propositions q1, q2 ...
- do not add any other propositions to this file
- do not load any third party library
- do not load any module such as Arith, Lists ...
- remove printing (Show, Locate, etc) before submitting

Failure to comply to one or more instructions can lead to grade of 0
of the whole exercise. Syntactically invalid files will be graded 0 *)

Lemma q1 :
  forall a b c: nat,
    a + (b + c) = (a + b) + c.
Proof.
  intros a b c.
  induction a; simpl; try reflexivity.
  induction b; simpl; rewrite <- IHa; simpl; reflexivity.
(* "rewrite FUNC" <=> "rewrite -> FUNC" *)
(* D'où le fait qu'on précise le sens *)
Qed.

(* Rappel : On applique la récurrence (induction) sur x
lorsque l'on veut prouver qu'une égalité (ou autre) est vraie pour tout rang de x *)

(* NB : "S" est l'opérateur successeur *)

Lemma q2 :
  forall a : nat,
    a + 0 = a.
Proof.
  intros a.
  induction a; simpl; try reflexivity.
  rewrite IHa; reflexivity.
Qed.

Lemma q3 :
  forall a b : nat,
    S (a + b) = a + S b.
(* On prouve la distributivité de l'addition par rapport à la succession (S) *)
Proof.
  intros a b.
  induction a; simpl; try reflexivity.
  rewrite IHa. reflexivity.
Qed.


Lemma q4 :
  forall a b : nat,
    a + b = b + a.
(* On prouve ici, la commutativité de l'addition *)
Proof.
  intros a b.
  induction a; simpl.
  - rewrite q2. reflexivity.
  - rewrite <- q3. rewrite IHa. reflexivity.
Qed.

Lemma q5 :
  forall a : nat,
    a * 0 = 0.
Proof.
  intros a.
  induction a; simpl; try reflexivity.
  assumption.
Qed.

Lemma q6 :
  forall n m: nat,
    m * S n = m + m * n.
Proof.
  intros n m.
  induction m; simpl; try reflexivity.
  rewrite q1, (q4 m n).
(* On précise l'ordre d'entrée des paramètres *)
(* "(q4 m n)" <=> "q4 with (a:=m) (b:=n)" *)
  rewrite IHm, q1. reflexivity.
Qed.

Lemma q7 : forall n m : nat,  m*n = n*m.
Proof.
  intros n m.
  induction m; simpl.
  - rewrite q5. reflexivity.
  - rewrite IHm, q6. reflexivity.
Qed.