# Exo 2.D-E

import numpy as np
from matplotlib import pyplot as plt

class LIF:
    def __init__(self, taum, vth, EL, gL, N, I):
        self.taum = taum
        self.vth = vth
        self.EL = EL
        self.gL = gL
        self.N = N
        self.I = I
    
    def df_dt(self, t, v):
        return (-(v - self.EL) + self.I / self.gL) / self.taum


dt = 0.1
tmax = 200
N = int(tmax // dt)

mod = LIF(10, -55, -75, 10, 2 / dt, 200.0001)

t = 0
vs = np.zeros((N, 1))
vs[0] = -60
refra = 0
spks = []
for i in range(1, N):
    if refra > 0:
        refra -= 1
        vs[i] = mod.EL
    else:
        vs[i] = vs[i-1] + mod.df_dt(t, vs[i-1]) * dt
        if vs[i] > mod.vth:
            spks.append(i)
            refra = mod.N
    t = t + dt

spks = np.array(spks)

ts = np.arange(N) * dt
plt.figure()
plt.plot(ts, vs)
plt.plot(ts[spks], vs[spks], "*m")

# Exo 3

import numpy as np
from matplotlib import pyplot as plt


class LoktaVolterra:
    def __init__(self, a, b, c, d, eps):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.eps = eps
    
    def df_dt(self, t, x):
        return np.array([self.a * x[0] - self.b * x[0] * x[1],
                         self.d * x[0] * x[1] - self.c * x[1]])
    def domega_dt(self):
        return self.eps * np.random.randn()


a = 0.5
b = 0.5
c = 0.5
d = 0.5
eps = 0.1

lv = LoktaVolterra(a,b,c,d,eps)


dt = 0.001
t = 0
tmax = 100
N = int(tmax // dt)
val = np.zeros((N, 2))
ts = np.zeros((N, 1))
val[0] = np.array([0.1, 0.2])
t = 0
for i in range(1, N):
    val[i,:] = val[i-1,:] + lv.df_dt(t,  val[i-1,:]) * dt +\
        lv.domega_dt() * np.sqrt(dt)
    t += dt
    ts[i] = t

plt.figure()
plt.plot(ts, val[:,0])
plt.plot(ts, val[:,1])