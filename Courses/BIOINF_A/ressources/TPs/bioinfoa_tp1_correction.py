# Exo 1.1.2.a

import numpy as np
from matplotlib import pyplot as plt

N0 = 50
g = 5

class Growth:
    def __init__(self, g):
        self.g = g
    def df_dt(self, t, x):
        return np.log(2) / self.g * x

def c(t):
    return N0 * 2 ** (t / g)
ts = np.arange(0, 60, 0.05)
plt.figure()
plt.plot(ts, c(ts))

gro = Growth(g)
dt = 0.5
t = 0
tmax = 60
N = int(tmax // dt)

val = np.zeros((N, 1))
ts = np.zeros((N, 1))
val[0] = N0
t = 0
for i in range(1, N):
    val[i] = val[i-1] + gro.df_dt(t,  val[i-1]) * dt
    t += dt
    ts[i] = t

plt.figure()
plt.plot(ts, val)
plt.plot(ts, c(ts), 'r--')