# Exo 1

import numpy as np
from matplotlib import pyplot as plt

class LoktaVolterra:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.update = np.array([
             [1,   0],
             [-1,  1],
             [0,  -1]]
            , dtype=int)

    def rates(self, pop):
        return np.array(
            [self.a * pop[0],
             self.b * pop[0] * pop[1],
             self.c * pop[1]])

lv = LoktaVolterra(10, 0.01, 10)
pop = [np.array([1000, 1000])]
ts = [0]
t = 0
tmax = 10

while t < tmax:
    cur_rates = lv.rates(pop[-1])
    tau = np.random.exponential(1 / np.sum(cur_rates))
    pick = np.cumsum(cur_rates) / np.sum(cur_rates)
    v = np.random.rand()
    k = np.min(np.where(pick >= v))
    t = t + tau
    ts.append(t)
    pop.append(pop[-1] + lv.update[k,:])

pop = np.array(pop)

plt.figure()
plt.plot(ts, pop[:,0])
plt.plot(ts, pop[:,1])

# Exo 3.2.1

import numpy as np
from matplotlib import pyplot as plt

class Brownian:
    def __init__(self, D):
        self.D = D
    def domega_dt(self):
        return np.sqrt(2 * self.D) * np.random.randn(2)

br = Brownian(1)
dt = 1e-4
tmax = 10
N = int(tmax // dt)
Xs = np.zeros((N,2))
Xs[0,:] = np.array([100, 100])

#Euler-Maruyama
for i in range(1, N):
    Xs[i,:] = Xs[i-1,:] + np.sqrt(dt) * br.domega_dt()

plt.figure()
plt.plot(Xs[:,0], Xs[:,1])

# Exo 3.2.2

import numpy as np
from matplotlib import pyplot as plt

class Brownian:
    def __init__(self, D):
        self.D = D
    def domega_dt(self):
        return np.sqrt(2 * self.D) * np.random.randn(2)

br = Brownian(1)
dt = 1e-4
tmax = 10
N = int(tmax // dt)


Nt = 50
trs = []
for k in range(Nt):
    Xs = np.zeros((N,2))
    Xs[0,:] = np.array([100, 100])
    for i in range(1, N):
        Xs[i,:] = Xs[i-1,:] + np.sqrt(dt) * br.domega_dt()
    trs.append(Xs)

MSD = np.zeros((N,1))
for i in range(1, N):
    tmp = []
    for j in range(Nt):
        tmp.append(np.sum((trs[j][i,:] - trs[j][0,:])**2))
    MSD[i] = np.mean(tmp)

ts = np.arange(N) * dt
plt.figure()
plt.plot(ts, MSD)
plt.plot(ts, 4 * br.D * ts, 'r--')