# Exo 1

import numpy as np
from matplotlib import pyplot as plt
from scipy.integrate import RK45

class HH:
    def __init__(self, Cm, gk, gNa, gl, Ek, ENa, El, I):
        self.Cm = Cm
        self.gk = gk
        self.gNa = gNa
        self.gl = gl
        self.Ek = Ek
        self.ENa = ENa
        self.El = El
        self.I = I

    def alpha_n(self, V):
        return 0.01 * (10 - V) / (np.exp((10 - V) / 10) - 1)
    def beta_n (self, V):
        return 0.125 * np.exp(-V / 80)
    def alpha_m(self, V):
        return 0.1 * (25 - V) / (np.exp((25 - V) / 10) - 1)
    def beta_m (self, V):
        return 4 * np.exp(-V / 18)
    def alpha_h(self, V):
        return 0.07 * np.exp(-V / 20)
    def beta_h(self,V):
        return 1 / (np.exp((30 - V) / 10) + 1)

    def d_dt(self, t, X):
        V,n,m,h = X
        return np.array([(self.I(t) - self.gk * n**4 * (V - self.Ek) -\
                          self.gNa * m**3 * h * (V - self.ENa) -\
                          self.gl * (V - self.El)) / self.Cm,
                         self.alpha_n(V) * (1 - n) - self.beta_n(V) * n,
                         self.alpha_m(V) * (1 - m) - self.beta_m(V) * m,
                         self.alpha_h(V) * (1 - h) - self.beta_h(V) * h])


dt = 0.001
Cm = 1
gNa = 100
gK = 5
gl = 0.3
ENa = 115
EK = -35
El = 10.6
tmax = 100

def I(t):
    if t > 5 and t < 20:
        return 500
    return 0


mod = HH(Cm, gK, gNa, gl, EK, ENa, El, I)

sol = RK45(mod.d_dt, 0, np.array([-65, 0, 0, 0]), tmax, max_step=dt)

ts = []
res = []
while sol.status != "finished":
    sol.step()
    ts.append(sol.t)
    res.append(sol.y)

res = np.array(res)

plt.figure()
plt.plot(ts, res[:,0])

plt.figure()
plt.plot(ts, res[:,1])
plt.plot(ts, res[:,2])
plt.plot(ts, res[:,3])
plt.legend(["n", "m", "h"])

# Exo 2.1.a-b

from matplotlib import pyplot as plt
from scipy.io import loadmat

#Question 2.a
#anat = loadmat("anat.mat")
#plt.figure()
#plt.imshow(anat["anat"][:,40,:], cmap="gray")

#Question 2.b 2.c
dat = loadmat("language_data.mat")

plt.figure()
for i in range(95):
    for j in range(9):
        plt.plot(range(189), dat["func"][i,j,50,:])

# Exo 2.c-d

import numpy as np
from matplotlib import pyplot as plt
from scipy.io import loadmat
from sklearn.linear_model import LinearRegression

dat = loadmat("language_data.mat")
X = dat["X"] #0 -> cste ; 1 -> phrases ; 2 -> non-mots ; > 2 : artéfacts
func = dat["func"]
voxels = dat["voxels"]


for i in range(voxels.shape[0]):
    x,y,z = voxels[i,:] - 1
    sig = func[x,y,z,:]
    
    regr = LinearRegression(fit_intercept=False)
    regr.fit(X, sig)
    b = regr.coef_
    
    recon = np.dot(X, b)
    
    #plt.figure()
    #plt.plot(range(189), sig)
    #plt.plot(range(189), recon, "r--")
    
    err = np.sum((sig - recon)**2)