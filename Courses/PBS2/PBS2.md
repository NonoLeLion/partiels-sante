# Probabilités et statistiques 📉

## Infos 💖

D'après le Moodle les DMs c'est 40% de la note. Donc faut juste gratter 2 puntos dans les deux trucs pour avoir la moyenne quoi 👀.  
Pour les probas le cours a l'air ok, donc go juste train [les TDs](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/probas/TDs) + [le cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/ressources/probas/PRBS2_cours.pdf) et c'est free.  
Parcontre du cotée des stats, l'ambiance est un peu bizarre. Même si bon, en vrai je pense qu'en faisant juste [les TDs](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/stats/TDs) ca le fait large 👍

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1205)  

Probabilités:
 - [Le cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/ressources/probas/PRBS2_cours.pdf) ⭐  
 - [Les TDs](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/probas/TDs) ⭐
 - [Rappelles: dépendance et compatibilité](http://perso.numericable.fr/mathlerebours/terminale/proba/compatibe_independant-cours.pdf) ⭐

 Statistiques:
  - [Slides du cours](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/stats/slides)
  - [Fiche stat.](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/stats/Fiche_STAT.pdf) ⭐  
  - [Les TDs](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/PBS2/ressources/stats/TDs) ⭐  
  - [IDE pour coder en R](https://beginr.u-bordeaux.fr/caps_1_1_installation.html) ⭐  
