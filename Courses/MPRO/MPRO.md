# Management et Pilotage de projets ✈️

## Infos 💖


### Modalités d'évaluation 📚
L'exam. est sur 18 pts (car 2 pts pour la présence aux TDs)

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1481) ⭐  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MPRO/ressources/slides) ⭐  
[TDs](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MPRO/ressources/TDs)