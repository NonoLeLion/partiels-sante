# H360 💤

## Infos 💖

## Links 🔗

[Moodle](https://moodle.epita.fr/course/view.php?id=2211)  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/H360&PMSI/ressources/slides) ⭐  

# PMSI 🥴

[Moodle](https://moodle.epita.fr/course/view.php?id=2213)  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/H360&PMSI/ressources/slides) ⭐  