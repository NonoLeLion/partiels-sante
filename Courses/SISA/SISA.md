# Systèmes d'information de Santé 🩺

## Infos 💖


## Links 🔗

[Moodle](https://moodle.epita.fr/course/view.php?id=2210)  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/SISA/ressources/slides) ⭐  
[Cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/SISA/ressources/cours) ⭐  