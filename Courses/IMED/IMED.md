# Imagerie Médicale - Hopital 🏥

## Infos 💖


## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1301)  
[Slides/Cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IMED/ressources/slides) ⭐  
[Les vidéos](https://epitafr.sharepoint.com/:f:/r/sites/2024SANTE/Documents%20partages/General/Recordings/IMED?csf=1&web=1&e=bGsQ7s) ⭐  