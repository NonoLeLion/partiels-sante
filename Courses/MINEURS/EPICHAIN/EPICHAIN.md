# EPICHAIN ⛓️

## Infos 💖

Slides et cheat sheet dispo pendant l'exam, avec normalement un acces à smartpy.io  
Certains codes dans les exercices ne marchent pas forcement, à cause de la version de smartpy qui n'est pas la bonne.  
Version de smartpy pour l'exam : https://releases.smartpy.io/0.19.0a3/ide

## Links 🔗

[Cheat Sheet](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MINEURS/EPICHAIN/ressources/EPICHAIN_cheat_sheet.pdf) ⭐  
[Exercices (pas complet encore)](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MINEURS/EPICHAIN/ressources/exercises) ⭐  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MINEURS/EPICHAIN/ressources/EPICHAIN_slides.pdf)  