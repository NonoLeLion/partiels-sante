I will try to summarize what we did
![Alt text](img/20_geocaching_01.png)

This is the version that directly stores and verifies the hash:

```py
import smartpy as sp

@sp.module
def main():
    class Geocaching(sp.Contract):
    
        def __init__(self, owner, deadline):
            self.data.treasures = sp.big_map({})
            self.data.score_per_player = sp.big_map({})
            self.data.current_winner = owner
            self.data.score_per_player[owner] = 0
            self.data.owner = owner
            self.data.deadline = deadline
            self.data.nb_treasures = 0

        @sp.entrypoint
        def create_treasure(self, password_hash):
            assert sp.now < self.data.deadline
            assert sp.sender == self.data.owner
            self.data.nb_treasures += 1
            self.data.treasures[self.data.nb_treasures] = sp.record(hash = password_hash, found = False)

        @sp.entrypoint
        def discover_treasure(self, id, password):
            assert sp.now < self.data.deadline
            treasure = self.data.treasures[id]
            assert not treasure.found
            assert sp.blake2b(password) == treasure.hash
            self.data.treasures[id].found = True
            if not self.data.score_per_player.contains(sp.sender):
                self.data.score_per_player[sp.sender] = 0
            self.data.score_per_player[sp.sender] += 1
            if self.data.score_per_player[sp.sender] > self.data.score_per_player[self.data.current_winner]:
                self.data.current_winner = sp.sender

        @sp.entrypoint
        def award_prize(self):
            assert sp.now >= self.data.deadline
            assert sp.sender == self.data.owner
            sp.send(self.data.current_winner, sp.balance)
                   
@sp.add_test(name = "Geocaching test")
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    carl = sp.test_account("carl").address
    scenario = sp.test_scenario(main)
    geocaching = main.Geocaching(alice, sp.timestamp(1000))
    scenario += geocaching
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 1"))).run(sender = alice, amount = sp.tez(100))
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 2"))).run(sender = alice)
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 3"))).run(sender = alice)
    geocaching.discover_treasure(id = 1, password = sp.pack("secret password 1")).run(sender = bob, now = sp.timestamp(100))
    geocaching.discover_treasure(id = 2, password = sp.pack("false password")).run(sender = bob, now = sp.timestamp(100), valid = False)
    geocaching.discover_treasure(id = 2, password = sp.pack("secret password 2")).run(sender = carl, now = sp.timestamp(100))
    geocaching.discover_treasure(id = 3, password = sp.pack("secret password 3")).run(sender = carl, now = sp.timestamp(200))
    geocaching.award_prize().run(sender = alice, now = sp.timestamp(100), valid = False)
    geocaching.award_prize().run(sender = carl, now = sp.timestamp(1000), valid = False)
    geocaching.award_prize().run(sender = alice, now = sp.timestamp(1000))
```

There was one flaw in this version, that I didn't talk about:

![Alt text](img/20_geocaching_02.png)

The fix for that flaw was simply to let the winner call the award_prize entrypoint themselves.  
So the version with this fixed is this one:

```py
import smartpy as sp

@sp.module
def main():
    class Geocaching(sp.Contract):
    
        def __init__(self, owner, deadline):
            self.data.treasures = sp.big_map({})
            self.data.score_per_player = sp.big_map({})
            self.data.current_winner = owner
            self.data.score_per_player[owner] = 0
            self.data.owner = owner
            self.data.deadline = deadline
            self.data.nb_treasures = 0

        @sp.entrypoint
        def create_treasure(self, password_hash):
            assert sp.now < self.data.deadline
            assert sp.sender == self.data.owner
            self.data.nb_treasures += 1
            self.data.treasures[self.data.nb_treasures] = sp.record(hash = password_hash, found = False)

        @sp.entrypoint
        def discover_treasure(self, id, password):
            assert sp.now < self.data.deadline
            treasure = self.data.treasures[id]
            assert not treasure.found
            assert sp.blake2b(password) == treasure.hash
            self.data.treasures[id].found = True
            if not self.data.score_per_player.contains(sp.sender):
                self.data.score_per_player[sp.sender] = 0
            self.data.score_per_player[sp.sender] += 1
            if self.data.score_per_player[sp.sender] > self.data.score_per_player[self.data.current_winner]:
                self.data.current_winner = sp.sender

        @sp.entrypoint
        def award_prize(self):
            assert sp.now >= self.data.deadline
            assert sp.sender == self.data.owner
            sp.send(self.data.current_winner, sp.balance)
                   
@sp.add_test(name = "Geocaching test")
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    carl = sp.test_account("carl").address
    scenario = sp.test_scenario(main)
    geocaching = main.Geocaching(alice, sp.timestamp(1000))
    scenario += geocaching
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 1"))).run(sender = alice, amount = sp.tez(100))
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 2"))).run(sender = alice)
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 3"))).run(sender = alice)
    geocaching.discover_treasure(id = 1, password = sp.pack("secret password 1")).run(sender = bob, now = sp.timestamp(100))
    geocaching.discover_treasure(id = 2, password = sp.pack("false password")).run(sender = bob, now = sp.timestamp(100), valid = False)
    geocaching.discover_treasure(id = 2, password = sp.pack("secret password 2")).run(sender = carl, now = sp.timestamp(100))
    geocaching.discover_treasure(id = 3, password = sp.pack("secret password 3")).run(sender = carl, now = sp.timestamp(200))
    geocaching.award_prize().run(sender = alice, now = sp.timestamp(100), valid = False)
    geocaching.award_prize().run(sender = carl, now = sp.timestamp(1000), valid = False)
    geocaching.award_prize().run(sender = alice, now = sp.timestamp(1000))
```

The second flaw was this one:

![Alt text](img/20_geocaching_03.png)

But we can do even faster:

![Alt text](img/20_geocaching_04.png)

And here is the corresponding code:

```py
import smartpy as sp

@sp.module
def main():
    class Geocaching(sp.Contract):
    
        def __init__(self, owner, deadline_commit, deadline_reveal, deadline_dispute, deposit):
            self.data.treasures = sp.big_map({})
            self.data.score_per_user = sp.big_map({})
            self.data.commits = sp.big_map({})
            self.data.current_winner = owner
            self.data.score_per_user[owner] = 0
            self.data.owner = owner
            self.data.deposit = deposit
            self.data.deadline_commit = deadline_commit
            self.data.deadline_reveal = deadline_reveal
            self.data.deadline_dispute = deadline_dispute
            self.data.nb_treasures = 0

        @sp.entrypoint
        def register_player(self):
            assert sp.now < self.data.deadline_commit
            assert not self.data.score_per_user.contains(sp.sender)
            assert sp.amount == self.data.deposit
            self.data.score_per_user[sp.sender] = 0
        
        @sp.entrypoint
        def create_treasure(self, password_hash):
            assert sp.now < self.data.deadline_commit
            assert sp.sender == self.data.owner
            self.data.nb_treasures += 1
            self.data.treasures[self.data.nb_treasures] = sp.record(hash = password_hash, found = False, player = self.data.owner, password = sp.bytes("0x00"))

        @sp.entrypoint
        def commit_discover_treasure(self, id, commit_data):
            assert sp.sender != self.data.owner
            assert sp.now <= self.data.deadline_commit
            assert not self.data.commits.contains(commit_data)
            self.data.commits[commit_data] = True
        
        @sp.entrypoint
        def reveal_discover_treasure(self, id, password):
            assert sp.now <= self.data.deadline_reveal
            treasure = self.data.treasures[id]
            assert not treasure.found
            self.data.treasures[id].found = True
            self.data.treasures[id].password = password
            self.data.treasures[id].player = sp.sender
            self.data.score_per_user[sp.sender] += 1
            if self.data.score_per_user[sp.sender] > self.data.score_per_user[self.data.current_winner]:
                self.data.current_winner = sp.sender

        # if someone detects that a reveal doesn't match a commitment, they can complain
        @sp.entrypoint
        def dispute_discovery(self, id):
            assert sp.now <= self.data.deadline_dispute
            treasure = self.data.treasures[id]
            assert treasure.found
            if sp.blake2b(treasure.password) == treasure.hash:
                expected_commit = sp.blake2b(sp.pack(sp.record(password = treasure.password, user = treasure.player)))
                assert not self.data.commits.contains(expected_commit)
            if self.data.score_per_user.contains(treasure.player):
                del self.data.score_per_user[treasure.player]
                sp.send(sp.sender, self.data.deposit)
            if treasure.player == self.data.current_winner:
                self.data.current_winner = self.data.owner
            self.data.treasures[id].found = False
            # we need to give more time for a player who really found it, to reveal it
            self.data.deadline_reveal = sp.add_seconds(self.data.deadline_reveal, 3600)
            self.data.deadline_dispute = sp.add_seconds(self.data.deadline_dispute, 3600)

        # If we eliminated the winner, we can't do a loop to recompute it, so we let the real winner claim their place
        @sp.entrypoint
        def update_winner(self):
            assert sp.now <= self.data.deadline_dispute
            assert self.data.score_per_user[sp.sender] > self.data.score_per_user[self.data.current_winner]
            self.data.current_winner = sp.sender

        @sp.entrypoint
        def claim_prize(self):
            assert sp.now > self.data.deadline_dispute
            assert sp.sender == self.data.current_winner
            sp.send(sp.sender, sp.balance)
                   
@sp.add_test(name = "Geocaching test")
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    carl = sp.test_account("carl").address
    scenario = sp.test_scenario(main)
    geocaching = main.Geocaching(alice, sp.timestamp(1000), sp.timestamp(2000), sp.timestamp(3000),sp.tez(1))
    scenario += geocaching
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 1"))).run(sender = alice, amount = sp.tez(100))
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 2"))).run(sender = alice)
    geocaching.create_treasure(sp.blake2b(sp.pack("secret password 3"))).run(sender = alice)

    geocaching.register_player().run(sender = bob, amount = sp.tez(1))
    geocaching.register_player().run(sender = carl, amount = sp.tez(1))
    
    # Correct commit and reveal for password 1
    commit_1 = sp.blake2b(sp.pack(sp.record(password = sp.pack("secret password 1"), user = bob)))
    geocaching.commit_discover_treasure(id = 1, commit_data = commit_1).run(sender = bob, now = sp.timestamp(100))
    geocaching.reveal_discover_treasure(id = 1, password = sp.pack("secret password 1")).run(sender = bob, now = sp.timestamp(100))

    # Try invalid password
    commit_fake = sp.blake2b(sp.pack(sp.record(password = sp.pack("false password"), user = bob)))
    geocaching.commit_discover_treasure(id = 2, commit_data = commit_fake).run(sender = bob, now = sp.timestamp(100))
    geocaching.reveal_discover_treasure(id = 2, password = sp.pack("false password")).run(sender = bob, now = sp.timestamp(100))
    geocaching.dispute_discovery(2).run(sender = carl)
    
    # Try to reveal without a commit
    geocaching.reveal_discover_treasure(id = 2, password = sp.pack("secret password 2")).run(sender = bob, now = sp.timestamp(100), valid = False)
    
    commit_2 = sp.blake2b(sp.pack(sp.record(password = sp.pack("secret password 2"), user = carl)))
    geocaching.commit_discover_treasure(id = 2, commit_data = commit_2).run(sender = carl, now = sp.timestamp(100))
    # Wrong person tries to reveal
    geocaching.reveal_discover_treasure(id = 2, password = sp.pack("secret password 2")).run(sender = bob, now = sp.timestamp(100), valid = False)
    geocaching.reveal_discover_treasure(id = 2, password = sp.pack("secret password 2")).run(sender = carl, now = sp.timestamp(100))

    
    commit_3 = sp.blake2b(sp.pack(sp.record(password = sp.pack("secret password 3"), user = carl)))
    geocaching.commit_discover_treasure(id = 2, commit_data = commit_3).run(sender = carl, now = sp.timestamp(100))
    geocaching.reveal_discover_treasure(id = 3, password = sp.pack("secret password 3")).run(sender = carl, now = sp.timestamp(200))
    geocaching.claim_prize().run(sender = alice, now = sp.timestamp(100), valid = False)
    geocaching.claim_prize().run(sender = carl, now = sp.timestamp(1000), valid = False)
    geocaching.claim_prize().run(sender = alice, now = sp.timestamp(1000), valid=False)
```

Hopefully wit this summary, eveything is clear for everyone. Don't hesitate to post questions here.

---

@2024_Mineure_blockchain There was an interesting question from someone, so I'll post my answer here.  
The question was about what type of real situations we can have like this where bots intercept transactions.

One example is the case of Arbitrage bots.

I have some content about that, that I will present later if we have time, but the basic idea is that you can have multiple decentralized exchange contracts where anyone can exchange tokens from one currency, into another currency.

But if two exchanges use different exchange rates between currencies A and B, you can make a little bit of money by purchasing tokens of currency A on the exchange where it's cheaper, then sell it on the exchange where it's more expensive. If you're the first one to notice it, you can make money in the process. That's called an arbitrage. The benefit of this is that once you do that, then this changes the exchange rates in such a way that they are both the same.

So there are bots that look for this kind of situations, arbitrage bots. Sometimes the situations are more complicated, so it's not always obvious to find arbitrage situations, and it requires a bit of work. But eventually once you find an arbitrage opportunity, you send a sequence of transactions, that should end up in some profit for you.

However, a bot could detect your transaction, and notice that if they simply do the same transaction, but before you (thanks to a higher fee), then they can get the profit instead of you, without doing all the work of detecting the arbitrage situation.  
In the end it causes situations where all the bots keep increasing the fees, and it makes fees on the network higher than necessary. 
Using a commit and reveal scheme as I described is one way to avoid this situation, but as it requires several steps, it's not ideal.

---

On a related topic, although it's not using the same approach as our exercise, here is a news article about "real world" use case involving geocaching:  
https://xtz.news/en/tezos/decathlon/tezos-blockchain-powers-decathlons-innovative-rockrider-geocaching-game/