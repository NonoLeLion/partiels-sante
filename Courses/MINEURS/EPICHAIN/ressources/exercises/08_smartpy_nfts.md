Video on NFTs: https://youtu.be/cdBG_RZU05Y

Using what you have learned so far, create your own single NFT contract in SmartPy.

Remember that an NFT:
- Is Uniquely identifiable
- Contains some information: metadata
- Has an owner
- Can be transferred

Reminders:
- A smart contract and its storage can be the NFT
- Every contract is unique and identified by its address
- The contract can store the owner of the NFT
- It can store the metadata, for example a string “[your name]’s first NFT”
- To make it transferable, your contract needs an entrypoint that:
    - Updates the address of the owner
    - Only lets the current owner initiate the transfer