Remember to post your answers in your private channel (and only there).
You may discuss the topic with others in this channel,  but don't provide the answer itself here.

1. Hashed passwords exercise: https://static-items.algorea.org/files/checkouts/8ee728ceb058e62fb230f15fa4ef1eab/2015/2015-FR-AL-12-hash-code/index.html

2. Hash table exercise: https://static-items.algorea.org/files/checkouts/4b897b3fff6fe467aca673a13fd4e4c1/2018/2018-FR-AL-06-hash-table/index.html

3. Stream cipher exercise: https://static-items.algorea.org/files/checkouts/8bcd481a4bd837334ddea635091cfb1c/2022/2022-FR-AL-07-stream_cipher/index.html

For later, if you want to play with using a stream cipher as a way to encrypt files, use this url:
https://static-items.algorea.org/files/checkouts/27021fe34a3c67048f827d9ab72551ea/2022-2023/2023-FR-05-stream-cipher/index.html?taskID=alkindi-task-stream-cipher&version=1