Video on arithmetics in SmartPy: https://youtu.be/hroSG6y6MLU

Exercise: write and test the yield-farming contract as presented in the video.

To set the delegate for a contract, call  sp.set_delegate(address_option).

Reminders:
- Arithmetic operators such as -, +, *, / (integer division), can be used for two values of the same type
- For values of different types or more, use sp.mod(a, b), sp.add(a,b), sp.mul(a,b), sp.ediv(a,b)
- Return types:
    - Addition, multiplication and integer division return nat if both operands are nat, and int if one operand is an int
    - Substraction and negation return an int whether operands are int or nat
    - Modulo always returns a nat
    - sp.ediv(a,b) returns an option on a pair of values (q,r): the quotient and remainder

- For mutez:
    - Addition and substraction of mutez gives a result of type sp.mutez
    - Multiplying mutez with nat (or vice-versa) gives a result of type sp.mutez
    - Additions and multiplications of mutez fail in case of overflows

- Pairs:
    - Create a pair using (-42, "example"), which is of type sp.pair[sp.int, sp.string]
    - Extract the first or second component of a pair: sp.fst(pair) or sp.snd(pair)
    - Extract the components in two variables (or more for a tuple) : (a, b) = (42, "abc")

- Type conversions:
    - mutez to nat: utils.mutez_to_nat(n)
    - nat to mutez: utils.nat_to_mutez(n) or utils.nat_to_tez(n)
    - nat to int: sp.to_int(nat)
    - int to nat: sp.as_nat(int)

- Computing percentages
    - sp.split_tokens(tezAmount, quantity, totalQuantity) returns tezAmount * quantity / totalQuantity

---

Here is the first example shown, that you can play around with (it doesn't work as is, so you will have to fix it).

```py
import smartpy as sp

@sp.module
def main():

    class Calculator(sp.Contract):
        def __init__(self):
            self.data.int_value = sp.int(1)
            self.data.nat_value = sp.nat(0)
        
        @sp.entrypoint
        def add(self, x, y):
            self.data.int_value = x + y
        
        @sp.entrypoint
        def sub(self, x, y):
            sp.cast(x, sp.int)
            self.data.int_value = x - y
        
        @sp.entrypoint
        def multiply(self, x, y):
            self.data.int_value = x * y
    
        @sp.entrypoint
        def negation(self, x):
            self.data.int_value = -x 
            
        @sp.entrypoint
        def divide(self, x, y):
            self.data.nat_value = x / y
    
        @sp.entrypoint
        def modulo(self, x, y):
            self.data.nat_value = sp.mod(x, y)
        
        @sp.entrypoint
        def shorthand(self, x):
            self.data.int_value += x
            #self.data.value -= x
            #self.data.value *= x

@sp.add_test()
def test():
    c1 = main.Calculator()
    sc = sp.test_scenario("Test", main)
    sc += c1
    c1.add(x = 5, y = 3)
    c1.sub(x = 5, y = 3)
    c1.multiply(x =  5, y = 3)
    c1.negation(3)
    c1.divide(x = 5, y = 3)
    c1.modulo(x = 8, y = 3)
    c1.shorthand(3)
```

Then play the one manipulating mutez (doesn't work as is either):

```py
import smartpy as sp

@sp.module
def main():

    class ComputingTez(sp.Contract):
        def __init__(self):
            self.data.financial_value = sp.tez(1)
        
        @sp.entrypoint
        def add(self, x, y):
            self.data.financial_value = x + y
        
        
        @sp.entrypoint
        def sub(self, x, y):
            self.data.financial_value = x - y
       
        @sp.entrypoint
        def multiply(self, x, y):
            self.data.financial_value = x * y
            
        @sp.entrypoint
        def divide(self, x, y):
            self.data.financial_value = x / y


@sp.add_test()
def test():
    c1 = main.ComputingTez()
    sc = sp.test_scenario("Test", [sp.utils,main])
    sc += c1
    c1.add(x = sp.tez(5), y = sp.tez(3))
    c1.sub(x = sp.tez(5), y = sp.tez(3))
    c1.multiply(x =  sp.tez(5), y = sp.nat(3))
    c1.divide(x =  sp.tez(5), y = sp.nat(3))
```

in the video, it seems like in a formula I wrote deposited_amount, when I meant deposit_amount 
I got this question: For the exercise 14 i don't understand the rampup_duration parameter can u explain it ?  
The idea is that if we start paying the user's interests immediately, we could lose money.  
Let's say the owner delegates to a baker and the next time this baker gets a reward is in 10 days. Someone could lend money to the contract, get it out 9 days later, and the owner  of the contract would have to pay 9 days of interest to them even though they never got any rewards from the baker.  
Having a "rampup_period" makes sure the owner starts collecting rewards before they need to pay interest

I made a mistake in the video, the type of rampup duration should be an int
in general, a duration should be an int, as it is the difference between two timestamps.  

Video of the solution
https://www.youtube.com/watch?v=vvsPjcF6dk0

---

```py
import smartpy as sp

@sp.module
def main():

    class YieldFarming(sp.Contract):
        def __init__(self, owner, lender, annual_yield_rate, ramp_up_duration):
            self.data.owner = owner
            self.data.lender = lender
            self.data.deposit_amount = sp.tez(0)
            self.data.deposit_date = sp.timestamp(0)
            self.data.annual_yield_rate = annual_yield_rate
            self.data.ramp_up_duration = ramp_up_duration
    
    
        @sp.entrypoint
        def owner_withdraw(self, requestedAmount):
            assert sp.sender == self.data.owner
            one_year_yield = sp.split_tokens(self.data.deposit_amount, self.data.annual_yield_rate, 100)
            reserve = self.data.deposit_amount + one_year_yield
            assert sp.balance - requestedAmount >= reserve
            sp.send(sp.sender, requestedAmount)
            
        @sp.entrypoint
        def set_delegate(self, delegate_option):
            assert sp.sender == self.data.owner
            sp.set_delegate(delegate_option)
    
        @sp.entrypoint
        def deposit(self):
            assert sp.sender == self.data.lender
            assert self.data.deposit_amount == sp.tez(0)
            self.data.deposit_amount = sp.amount
            self.data.deposit_date = sp.now
            
            
        @sp.entrypoint
        def withdraw(self):
            assert sp.sender == self.data.lender
            duration = sp.is_nat(sp.now - (sp.add_seconds(self.data.deposit_date, self.data.ramp_up_duration))).unwrap_some()
            one_year= 365*24*3600
            one_year_yield = sp.mul(self.data.deposit_amount, self.data.annual_yield_rate)
            duration_yield = sp.split_tokens(one_year_yield, duration, one_year*100)
            sp.send(sp.sender, self.data.deposit_amount + duration_yield )
            self.data.deposit_amount = sp.tez(0)
            self.data.deposit_date = sp.timestamp(0)
    
        @sp.entrypoint
        def default(self):
            pass

@sp.add_test()
def test():
    owner = sp.test_account("owner").address
    alice = sp.test_account("Alice").address
    bob = sp.test_account("Bob").address
    eve = sp.test_account("Eve").address
    delegate = sp.test_account("delegate")
    voting_powers = {
        delegate.public_key_hash: 0,
    }
    c1 = main.YieldFarming(owner = owner, lender = alice, annual_yield_rate = 4, ramp_up_duration = 3600*24*21)
    scenario = sp.test_scenario("Test", main)
    scenario += c1
    scenario.h3("Testing default entrypoint")
    c1.default(_sender = owner, _amount = sp.tez(5))
    scenario.verify(c1.balance == sp.tez(5))
    scenario.h3("Testing deposit entrypoint")
    c1.deposit(_sender = alice, _amount = sp.tez(100))
    scenario.verify(c1.data.deposit_amount == sp.tez(100))
    scenario.verify(c1.balance == sp.tez(105))
    c1.deposit(_sender = alice, _amount = sp.tez(100), _valid = False)
    scenario.h3("Testing withdraw entrypoint")
    scenario.h3("Testing owner_withdraw entrypoint")
    c1.owner_withdraw(sp.tez(1), _sender = bob, _valid = False)
    c1.owner_withdraw(sp.mutez(1000001), _sender = owner, _valid = False)
    c1.owner_withdraw(sp.tez(1), _sender = owner)
    scenario.h3("Testing set delegate")
    c1.set_delegate(sp.some(delegate.public_key_hash), _sender = owner, _voting_powers = voting_powers)
    c1.withdraw(_sender = alice, _now = sp.timestamp(3600*24*21 + 3600*24*365 ))
```