Video on options in SmartPy: https://youtu.be/lygGT5WyrcA

Note that the syntax sp.unwrap_some(myOption) is incorrect, it should now be myOption.unwrap_some()

Modify the NFT for sale contract so that the NFT is not always on sale

Use an option so that the NFT is not always on sale:
- Use set_price() with None if you don’t want it to be on sale
- Set the price to None right after the sale, so that it can’t be bought again immediately

Reminders:
- You can use sp.some(value) or None to create an option
- Two options can be compared for equality:assert myOption == sp.Some(42) or assert myOption != None
- You can extract the value by unwrapping the option:v = myOption.unwrap_some()
- Any other operation, such as adding two values, requires unwrapping the options first
- You can create options on most types, including complex ones (that we will see later)
- Only use options when they really help or when there is no choice. They may unnecessarily complexify the code.
- Use nobody = sp.address("KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT") if you need an invalid address
- When calling an emtrypoint from a test, if you want to pass None, you need to use sp.none. This will most likely change soon, so that you can use None there as well

---

A solution (but without proper tests):

```py
import smartpy as sp

@sp.module
def main():

    class NftForSale(sp.Contract):
    
       def __init__(self, owner, metadata, price):
           self.data.owner = owner
           self.data.metadata = metadata
           self.data.price = price
           
       @sp.entrypoint
       def set_price(self, new_price):
           assert sp.sender == self.data.owner, "you cannot update the price"
           self.data.price = new_price
    
       @sp.entrypoint
       def buy(self):
           assert sp.Some(sp.amount) == self.data.price
           sp.send(self.data.owner, sp.amount)
           self.data.owner = sp.sender
           self.data.price = None
    
@sp.add_test()
def test():
    alice = sp.test_account('alice').address
    bob = sp.test_account('bob').address
    c1 = main.NftForSale(owner = alice, metadata = "My NFT", price = sp.Some(sp.mutez(5000000)))
    scenario = sp.test_scenario("Test", main)
    scenario +=c1
    c1.set_price(sp.Some(sp.mutez(7000000)), _sender = alice)
    c1.buy(_sender = bob, _amount = sp.mutez(7000000))
```