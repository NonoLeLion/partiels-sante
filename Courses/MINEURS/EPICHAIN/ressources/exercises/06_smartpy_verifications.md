Video on verifications in SmartPy: https://youtu.be/u98Z6oGgwOQ

Video on using multiple values: https://youtu.be/8316qAQqCs8

1) Add verifications to the endless wall contract from the previous exercise.

People may only add strings with a length between 3 and 30 characters included.

2) Add tests that make sure that your contract does this well

To obtain the length of a string, use: sp.len("a string")

3) Modify the contract wall so that it also keeps track of the number of calls

4) Modify the contract by adding a name parameter to write_message, so that users also send their names, between 3 and 15 characters.
Each call should add ", [Name]: [Message]". For example ", Mathias: Hello"

Reminders:
- use assert [condition] to perform a check
- use assert [condition], "[error message"]  to include an error message in the case of a failure
- you may use regular comparison operators: <, >, <=, >=, !=, ==
- you may use and, or, not but also ^ (exclusive or) operators.

In the test scenario:
- to check that a call fails when it should: c1.my_entrypoint(params).run(valid = False)
- to check the error message: c1.my_entrypoint(params).run(valid=False, exception="My error")

When calling an entrypoint from your test scenario:
- if the entrypoint has a single parameter, pass only the value: contract.entryPoint(value)
- if there are multiple parameters, pass their names: contract.entryPoint(name1 = v1, name2 = v2)

---

Someone asked how we could debug by printing some values within a smart contract.
The answer is that you can use trace(value), it will output the value in the console of the browser.
if you want to print two values at once, you can print pairs:
trace(("nb_calls", value))
a pair of two values a and b can be written as (a, b) 

Most people who are done with 6.4 are missing the same thing.

If you have a comparison like a <= 30 in your code, make sure you do at least two tests, one for each side of the limit:
- one with a = 30
- one with a = 31
This way you detect the most common type of bugs: off-by-one bugs, for example where you write a < 30 instead of a <= 30.

sorry it was trace not sp.trace (at least in this version of SmartPy)