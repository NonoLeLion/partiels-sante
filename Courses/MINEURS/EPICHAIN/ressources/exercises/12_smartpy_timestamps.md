Video on timestamps in SmartPy: https://www.youtube.com/watch?v=k5UmlsZajCY

Change your NFT contract so that it can't be sold again less than 5 days since it was last bought.

Reminders:
- The current time is not available from a contract, as every node needs to get the same result
- Instead, a contract can access the time of the current block
- This time is predictable based on information from the previous block
- Timestamps are a number of seconds since 01/01/1970, and can be infinitely negative or positive
- Timestamps can be created in three ways:
    - Using a number of seconds since 01/01/1970: sp.timestamp(42000)
    - Using a list of parameters: sp.timestamp_from_utc(year, month, day, h, m, s)
    - Getting the timestamp of the current block: sp.now
- You can add time to a timestamp: d = sp.add_seconds(a, 42)
- Compare two timestamps: assert sp.now <= deadline, "too late"
- Compute the difference between two timestamps: assert sp.now - lastCall > 60, "too soon"
- When testing, time doesn't pass automatically, and is 0 by default. You have to specify it in your calls:myContract.myEntryPoint(param).run(now = sp.timestamp(4000))

for checking if 5 days are passed, don't do this:
``assert sp.now - self.data.time > 24 * 60 * 60 * 5, "Too early"``

Instead, do this:
``assert sp.now >= sp.add_days(self.data.time, 5), "Too early"``

---

```py
import smartpy as sp

@sp.module
def main():

    class NftForSale(sp.Contract):
    
       def __init__(self, owner, metadata, price, buy_date):
           self.data.owner = owner
           self.data.metadata = metadata
           self.data.price= price
           self.data.buy_date = buy_date
           
       @sp.entrypoint
       def set_price(self, new_price):
           assert sp.sender == self.data.owner, "you cannot update the price"
           self.data.price = new_price
    
       @sp.entrypoint
       def buy(self):
           assert sp.amount == self.data.price
           assert sp.now >= sp.add_days(self.data.buy_date, 5)  , "5 days between each buy"
           sp.send(self.data.owner, self.data.price)
           self.data.owner = sp.sender
           self.data.buy_date = sp.now
    
@sp.add_test()
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    eve = sp.test_account("eve").address
    c1 = main.NftForSale(owner=alice, metadata='my first NFT', price=sp.mutez(5000000), buy_date = sp.timestamp(0))
    scenario = sp.test_scenario("Test", main)
    scenario += c1
    scenario.h3("only owner can set price")
    c1.set_price(sp.mutez(7000000), _sender = alice)
    c1.set_price(sp.mutez(8000000), _sender = bob, _valid = False)
    scenario.h3("Checking deadline")
    c1.buy(_sender = eve, _amount = sp.mutez(7000000), _now = sp.timestamp(4*24*3600), _valid = False)
    c1.buy(_sender = bob, _amount = sp.mutez(7000000), _now = sp.timestamp(6*24*3600), _valid = True)
    scenario.verify(c1.data.owner == bob)
```

(It's missing the _exception parameters in the test, that you should always have)