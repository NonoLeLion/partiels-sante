Here is the video on records: https://youtu.be/7a1xoZcNypw

Recap:
- create a record using sp.record(name1 = value1, name2 = value2, etc.)
- the record type is sp.record(name1 = type1, name2 = type2, etc.)
- you can access fields like this: my_record.name1
- you can create a type abbreviation in the main module but outside of any contract, like this: tRecord : type = sp.record(name1 = type1, name2 = type2, etc.) then use "tRecord" everywhere instead of the whole type

Exercise:
- change the dynamic NFT contract as described at the end of the video

---
Video of the solution: https://www.youtube.com/watch?v=IV98G0bP4qk

```py
import smartpy as sp

@sp.module
def main():

    class NftForSale(sp.Contract):    
        def __init__(self, owner, metadata, price):
            self.data.owner = owner
            self.data.metadata = metadata
            self.data.price = price
            self.data.deadline = sp.timestamp(0)
            self.data.last_training = sp.timestamp(0)
    
        @sp.entrypoint
        def set_price(self, new_price, deadline):
            assert sp.sender == self.data.owner, "you cannot update the price"
            self.data.price = new_price
            self.data.deadline = deadline
    
        @sp.entrypoint
        def buy(self):
            assert sp.amount == self.data.price, "wrong price"
            assert sp.now <= self.data.deadline
            sp.send(self.data.owner, self.data.price)
            self.data.owner = sp.sender

        @sp.entrypoint
        def feed(self):
            assert sp.amount == sp.tez(1)
            self.data.metadata.size += 1

        @sp.entrypoint
        def train(self):
            assert self.data.metadata.size > 10
            assert sp.add_days(self.data.last_training, 1) <= sp.now
            self.data.last_training = sp.now
            self.data.metadata.size -= 1
            self.data.metadata.strength += 1

@sp.add_test()
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    c1 = main.NftForSale(owner = alice,
                         metadata = sp.record(name = "Beast",
                                              size = sp.int(10),
                                              strength = sp.int(0)),
                         price = sp.mutez(5000000))
    scenario = sp.test_scenario("Test", main)
    scenario += c1
    scenario.h3(" Testing set_price entrypoint")
    #testing set price
    c1.set_price(new_price = sp.tez(7), deadline = sp.timestamp(10), _sender = alice)
    c1.feed(_amount = sp.tez(1), _sender = alice)
```

Note that sometimes I fix things during the video, and it could happen that the code I post is not the latest one. If you think you see a bug, don't hesitate to say it. I sometimes make mistakes too.  
And a lot of times my own tests are incomplete (this will get worse in the next modules). So don't take my tests as a good example of what to do.