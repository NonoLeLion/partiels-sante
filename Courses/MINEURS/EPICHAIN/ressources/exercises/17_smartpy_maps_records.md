Video on maps and records in SmartPy: https://www.youtube.com/watch?v=81vrGznJr6I

As described in the video, implement a contract that can handle multple NFTs 
Reminders:

Records
- Creating a record: aRecord = sp.record(field1 = 1, field2 = "A string")
- Accessing a field: v = aRecord.field1
- Creating a record type: sp.record(field1 = sp.nat, field2 = sp.string)
- Type abbreviations, in the main() module but outside of a contract: tRecord : type = sp.record(field1 = sp.nat, field2 = sp.string)

Maps:
- Creating a map with some data: my_map = {0 : "aa", 1 : "bb" }
- Empty map : my_map = {}
- Create an empty map but specify the type: my_map = sp.cast({}, sp.map[sp.int, sp.string])
- Adding/updating entries: my_map[0] = "cc"
- Accessing entry value: v = my_map[1]
- Deleting entries: del my_map[1]
- Check the existence: my_map.contains(1)
- Getting the size: sp.len(my_map)
- Getting list of all entries: my_map.items()  
returns lists of records: [{ key: 1, value: "aa" }, { key: 2, value: "bb" }]
- Getting list of all keys: my_map.keys()
- Getting list of all values: my_map.values()

Storage and gas cost issues:
- the storage is serialized/deserialized between calls. This consumes CPU
- We estimate the amount of CPU time consumed using Gas
- Calling a transaction implies paying for this gas as part of the fee for the baker
- There is a limit to how much gas can be consumed in a single transaction
- A contract can be stuck for ever if we exceed this limit
- Some types have unlimited sizes: int, nat, string, timestamps, maps


Big_maps:
- entries in a big map are serialized/deserialized on-demand
- the syntax is the same except using sp.big_map instead of sp.map, and sp.big_map({}) to create an empty one
- the length, list of keys, items or values are not available for big_maps

---

![Alt text](img/17_smartpy_maps_records.png)
Here is the structure I suggest you use for now.  
The first bulletpoint in the buy entrypoint is not necessary.  
This version has the flaw that the author could block the sales, but I suggest you start with this version, and once it works, you can try to see how to fix the flaw

Solution video: https://youtu.be/X8u1ADBEDis  
It's currently being processed by youtube, so will be available maybe in 15 minutes

Several people asked if there was a way to check if an address would accept the tez, and fo something like:
```py
if sp.accepts_tez(author):
    sp.send(author, sp.tez(1))
```
there is no such possibility unfortunately