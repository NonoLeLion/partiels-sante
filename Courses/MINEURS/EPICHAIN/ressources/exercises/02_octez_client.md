Warning: as soon as the video shows you how to configure the gitpod settings, pause the video and do it, so that the gitpod may load.

Here is the link. Don’t open it until you have changed your settings.

https://gitpod.io/#https://gitlab.com/nomadic-labs/training-gitpod

Warning: don’t copy the commands by hand from the videos. Instead, use the ones provided in the cheat sheet, to make sure you don’t make mistakes, and the version you use is up to date.

Watch the video on Octez client: https://www.youtube.com/watch?v=3sLUdnyhLM0

Please note that tezos-client  has been renamed octez-client, but the video has not been updated since the name change.

Here is a link to the Octez Client Cheat Sheet: 
https://docs.google.com/document/d/1K3D0tZFw40-rH3gBhjVVLuP3gRrHL922YvSz85svMgg/edit?usp=sharing

Exercise Client.1
Configure your octez client as shown on the video:

- select VS Code for Browser as your IDE in gitpod settings
- open a new gitpod workspace
- point your octez-client to the correct endpoint (see cheat sheet)
- create an account
- use a faucet to send tez to your account
- observe this account on tzkt.io (select ghostnet in the top-left corner)
- share the public key hash of your account, with other participants (it starts with tz1)
- setup some contract aliases on the accounts of other participants
- setup environment variables, and a configuration file