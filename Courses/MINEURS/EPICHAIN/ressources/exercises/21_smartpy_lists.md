Video about lists: https://youtu.be/0pHrQAuwGcQ

![Alt text](image.png)

Start with this contract:

```py
import smartpy as sp

@sp.module
def main():

    class NftForSale(sp.Contract):
    
       def __init__(self, owner, metadata, price):
           self.data.owner = owner
           self.data.metadata = metadata
           self.data.price = price
    
       @sp.entrypoint
       def set_price(self, new_price):
           assert sp.sender == self.data.owner, "you cannot update the price"
           self.data.price = new_price
    
       @sp.entrypoint
       def buy(self):
           assert sp.amount == self.data.price, "wrong price"
           sp.send(self.data.owner, self.data.price)
           self.data.owner = sp.sender
        
    class NFTJointAccount(sp.Contract):
        def __init__(self, owner1, owner2):
            self.data.owner1 = owner1
            self.data.owner2 = owner2
    
        @sp.entry_point
        def buy_nft(self, nft_address):
            assert (sp.sender == self.data.owner1) or (sp.sender == self.data.owner2)
            c = sp.contract(sp.unit, nft_address, entrypoint="buy").unwrap_some()
            sp.transfer((), sp.amount, c)
        
        @sp.entrypoint
        def set_nft_price(self, nft_address, new_price):
            assert (sp.sender == self.data.owner1) or (sp.sender == self.data.owner2)
            nft_contract = sp.contract(sp.mutez, nft_address, entrypoint="set_price").unwrap_some()
            sp.transfer(new_price, sp.tez(0), nft_contract)
```

Solution video: https://www.youtube.com/watch?v=9GTiWYBPlFM