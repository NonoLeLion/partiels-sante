Video about inter-contract calls:
https://www.youtube.com/watch?v=SWdbKiZ_Pu0
Reminders:
Syntax to call another contract:
```py
contract = sp.contract([parameter type], [contract address], entrypoint = "[entrypoint name]").unwrap_some()
sp.transfer([parameter value], amount, contract)
```

In the test:
```py
destination = main.DestinationContract([constructor parameters])
caller = main.CallerContract([constructor parameters])
caller.callContract(destination.address)
```

---

![Alt text](img/18_inter_contract_calls_01.png)
![Alt text](img/18_inter_contract_calls_02.png)

the NFTForSale contract is this one:
```py
    class NftForSale(sp.Contract):
       def __init__(self, owner, metadata, price):
           self.data.owner = owner
           self.data.metadata = metadata
           self.data.price = price
         
    
       @sp.entrypoint
       def set_price(self, new_price):
           assert sp.sender == self.data.owner, "not the owner"
           self.data.price = new_price
    
       @sp.entrypoint
       def buy(self):
           assert sp.amount == self.data.price, "wrong price"
           sp.send(self.data.owner, self.data.price) 
           self.data.owner = sp.sender
```

Actually it makes more sense to use this one:

```py
    class NftForSale(sp.Contract):
        def __init__(self, owner, metadata, price, author_rate, author):
            self.data.owner = owner
            self.data.metadata = metadata
            self.data.price = price
            self.data.author_rate = sp.nat(5)
            self.data.author = author
               
        @sp.entrypoint
        def buy(self):
           assert sp.amount == self.data.price
           owner_share = sp.split_tokens(self.data.price, abs(100 - self.data.author_rate), 100)
           sp.send(self.data.owner, owner_share)
           self.data.price += sp.split_tokens(sp.amount, 10, 100)
           self.data.owner = sp.sender
    
        @sp.entrypoint
        def claim_author_rate(self):
            assert sp.sender == self.data.author, " not your money "
            sp.send(self.data.author, sp.balance)
```

Solution video: https://youtu.be/GPJuy2RtFj8