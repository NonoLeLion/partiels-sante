Video on types in SmartPy: https://youtu.be/w33axEyIdp0

This contract doesn't compile. Fix the issue and make it compile:

```py
import smartpy as sp

@sp.module
def main():

    class StoreValue(sp.Contract):
       def __init__(self):
           self.data.storedValue = 24
    
       @sp.entrypoint
       def add(self, a):
          self.data.storedValue += a
    
       @sp.entrypoint
       def sub(self, b):
           self.data.storedValue -= b

@sp.add_test(name = "Testing")
def test():
   scenario = sp.test_scenario(main)
   contract = main.StoreValue()
   scenario += contract
   scenario.h3(" Helping type inference ")
   contract.add(sp.nat(5))
   contract.sub(5)
```



Then, add a multiply entrypoint:
- it should take a parameter called factor of type int.
- it should multiply the value of the storage by this parameter

Finally, add corresponding tests to your scenario.
Reminders:
- There are two types on Tezos to manipulate simple numbers: sp.int and sp.nat
- sp.nat are for non-negative values
- there is no upper limit (or lower limit for `sp.int), so no risk of overflow errors, but using storage costs tez
- there are no floating point numbers on Tezos. This is on purpose, to avoid any rounding issues
- we can convert nat into int using sp.to_int(value)
- we can convert a postitive int into nat using sp.as_nat(value)
- Type inference can infer the types of values based on:
    - the initial value assigned to the storage
    - values passed as parameters in tests
    - operations applied to parameters
- There are sveral ways to help type inference:
    - add information about the type of a parameter, using sp.cast(name, type) within an entrypoint
    - provide the type values in the storage or parameter calls using sp.int(5) or sp.nat(5)

---

Here is one possible solution:
```py
import smartpy as sp

@sp.module
def main():

    class StoreValue(sp.Contract):
       def __init__(self):
           self.data.storedValue = sp.nat(24)
    
       @sp.entrypoint
       def add(self, a):
           # sp.cast(a, sp.nat)
           self.data.storedValue += a
    
       @sp.entrypoint
       def sub(self, b):
           tmp = self.data.storedValue - b
           self.data.storedValue = sp.as_nat(tmp)

       @sp.entrypoint
       def multiply(self, factor):
           self.data.storedValue *= factor

@sp.add_test(name = "Testing")
def test():
    scenario = sp.test_scenario(main)
    contract = main.StoreValue()
    scenario += contract
    scenario.h3(" Helping type inference ")
    contract.add(sp.nat(5))
    contract.sub(5)
    scenario.verify(contract.data.storedValue == 24)
    contract.multiply(4)
    scenario.verify(contract.data.storedValue == 96)
    contract.multiply(2)
    scenario.verify(contract.data.storedValue == 192)
```

Another possibility was to simply replace sp.nat with sp.int in the scenario  
key issues were:
- sp.cast(a, sp.int) doesn't convert a to an int but declares that a is of type int
- make sure you add verifications in your scenario after the entrypoint calls
- it's important to do multiple tests for each entrypoint, as it is easy to do an entrypoint that works the first time, but not the second time.