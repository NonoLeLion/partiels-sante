Below is a contract, written in the new (beta) version of SmartPy, that lets people mint NFTs, then put them on Auction for other people to buy.

The idea is that :
- Anyone can mint a new NFT with some metadata. They are the initial owner.
- Minting costs 1 tez (so that the creator of this contract and the dApp can profit)
- Then they can put it on sale, for the highest bidder. They set a deadline, and whoever bids the most gets to purchase the NFT from them
- To bid for a given NFT, you have to pay more than the previous bidder (initially the seller, who bids 0 tez by default). Your tez stay in the
- If someone bids more than you for a given NFT, you get reimbursed and they become the new topBidder
- When the deadline expires, the seller of the NFT can get the payment, and the ownership of the NFT is transferred to the top Bidder

```py
import smartpy as sp

@sp.module
def main():

    class Auction(sp.Contract):
        def __init__(self):
            self.data.items = {}   # a map of items on sale. For each tokenID, a seller, deadline, topBid and topBidder
            self.data.tokens = {}  # a map that stores metadata and the owner of each tokenID
            self.data.tokenID = 1  # a counter that will increment every time someone mints a new NFT

        @sp.entry_point
        def mint(self, metadata):
            sp.cast(metadata, sp.string)
            assert sp.amount == sp.tez(1) # when minting, you pay 1 tez for the service
            self.data.tokens[self.data.tokenID] = sp.record(metadata = metadata, owner = sp.sender)
            self.data.tokenID += 1
        
        @sp.entrypoint
        def openAuction(self, itemID, seller, deadline):
		    # We create an oction for a given token. People can bid until a deadline
			# We keep track of the topBid and topBidder. The seller is the initial topBidder for 0 tez.
            self.data.items[itemID] = sp.record(seller = seller,
                                                deadline = deadline,
                                                topBid = sp.tez(0),
                                                topBidder = seller
                                               )
    
        @sp.entrypoint
        def bid(self, itemID):
            item = self.data.items[itemID]
			# we check that they bid more than the current top Bid, and that the deadline is not expired
            assert sp.amount > item.topBid
            assert sp.now < item.deadline
			
			# We reimburse the previous top bidder, and update the topBid and topBidder
            sp.send(item.topBidder, item.topBid)
            item.topBid = sp.amount
            item.topBidder = sp.sender
            self.data.items[itemID] = item

        @sp.entrypoint
        def claimTopBid(self, itemID):
		    # After the deadline, the seller can get the value of the top bid, and the ownership is transferred
            item = self.data.items[itemID]
            assert sp.now >= item.deadline
            assert sp.sender == item.seller
            sp.send(item.seller, item.topBid)
            self.data.tokens[itemID].owner = item.topBidder

@sp.add_test()
def test():
    seller1 = sp.test_account("seller1").address
    seller2 = sp.test_account("seller2").address
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    scenario = sp.test_scenario("Test", main)
    auctionContract = main.Auction()
    scenario += auctionContract
    auctionContract.mint("Mon NFT", _sender = seller1, _amount = sp.tez(1))
    auctionContract.openAuction(itemID = 1, seller = seller1, deadline = sp.timestamp(100))
    auctionContract.bid(1, _sender = alice, _amount = sp.tez(1), _now = sp.timestamp(1))
    auctionContract.bid(1, _sender = bob, _amount = sp.tez(2), _now = sp.timestamp(2))
    auctionContract.claimTopBid(1, _sender = seller1, _now = sp.timestamp(101))
```

There are several flaws in this contract. Your goal is to find as many as possible, and think of a way to fix each of them

None of the bugs are specific to the language used. We'll later create the exact same contract in Archetype and JSLigo, with the exact same logic and flaws.

Hopefully the code is easy enough to read, with the help of comments. Don't hesitate to ask if you don't understand some of the syntax.

Here is the page about reentrancy flaws if you're interested: https://opentezos.com/smart-contracts/avoiding-flaws/#10-re-entrancy-flaws

---

![Alt text](img/23_auction_flaws.png)

- Solution for bug 2 is to not send the tez directly back to the previous top bidder, but instead use a ledger that keeps track of who is owed what, and let the previous top bidder claim their own tez. This way they can't bid with the address of a contract that has a default entrypoint that fails and could prevent tez from being sent to them, therefore preventing someone else from bidding after them.  
- Solution for bug 3 was to allow anyone to call claimTopBid, not just the seller. We also needed to use the ledger rather than directly sending tez to their address  
- Solution for bug 4 was to delete the entry from the items big_map so that a second call wouldn't work  
- Solution for bug 5 was to also use a ledger and assign the 1 tez to the owner of the contract in that ledger  
- Solution for bug 6 was simply to use big_maps instead of maps  
- Solution for bug 7 was simply to check if there is already an auction for a given itemID before creating a new one