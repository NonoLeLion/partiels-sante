We will switch to the latest version of SmartPy from now on:
https://releases.smartpy.io/0.19.0a3/ide

Main changes:
- when you create a test, you don't provide its name in the add_test function anymore, you leave it with no parameter: @sp.add_test(). Instead, you provide the name when you create your scenario: sp.test_scenario("Test", main)
- when you call an entrypoint, you don't use .run anymore to pass special values like sender, valid, exception or amount: you pass them directly as extra parameters of the contract call, like this: my_contract.my_entry_point(param1 = value, param2 = value, _sender = alice, _amount = sp.tez(5), _valid = False, _exception = "Not enough money")
- for options, they are now manipulated in the same way in the tests as in the contract, just use None for example, not sp.none

Exercise:
- convert your solution to ⁠10-smartpy-options so that it works in the new version of SmartPy