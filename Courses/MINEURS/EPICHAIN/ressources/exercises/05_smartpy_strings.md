Video on Strings in SmartPy: https://youtu.be/zULCqux6-cw

To illustrate the concepts of eternity and immutability, create this smart contract:

- Name the contract EndlessWall
- The  storage should contains a  field wallText of type string
- Add an entrypoint write_message that takes a parameter message of type string
    - It should add a comma, then the message, then the string " forever" to the storage.
    - Example: if the storage is "Hello" and we call write_message("Tezos"),
    - The new storage should become "Hello, Tezos forever”
- Create a corresponding test scenario


Reminders:
- string literals are simply written between double quotes like this: "Tezos"
- On Tezos, strings may only contain pure ASCII characters. No accents / unicode. This is on purpose.
- You may simply use the + operator to concatenate strings
- the name of the type is sp.string, but you probably won't need to specify it
mathias — 26/09/2023 17:11

Python devs may be tempted to use this syntax:
```py
f", {str} forever"
```
This is not available in smart contracts. SmartPy looks like python but is not python, and there's a lot of python that's not available, including this syntax

---

Here is one solution:

```py
import smartpy as sp

@sp.module
def main():
    class EndlessWall(sp.Contract):
        def __init__(self):
            self.data.wallText = "Hello"
        
        @sp.entrypoint
        def write_message(self, message):
            self.data.wallText += ", " + message + " forever"

@sp.add_test(name = "Testing")
def test():
    scenario = sp.test_scenario(main)
    contract = main.EndlessWall()
    scenario += contract
    scenario.h3("Testing write_message entrypoint")
    contract.write_message("Tezos")
    scenario.verify(contract.data.wallText == "Hello, Tezos forever")
    contract.write_message("SmartPy")
    scenario.verify(contract.data.wallText == "Hello, Tezos forever, SmartPy forever")
```

key issues were:
don't forget to verify the storage in the scenario after the call to the entrypoint
do at least two calls to each entrypoint in your scenario, and use different values of the parameter, to make sure the parameter is used correctly