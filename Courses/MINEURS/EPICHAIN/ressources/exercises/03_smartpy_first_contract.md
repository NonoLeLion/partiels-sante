Video on first contracts in Smartpy: https://youtu.be/5hg5EykWLII

Here is the example FlipValue contract presented in the course:
```py
import smartpy as sp

@sp.module
def main():

    class FlipValue(sp.Contract):
        def __init__(self):
            self.data.side = 0
        
        @sp.entrypoint
        def flip(self):
            self.data.side = 1 - self.data.side

@sp.add_test(name = "Testing")
def test():
    scenario = sp.test_scenario(main)
    c1 = main.FlipValue()
    scenario += c1
    scenario.h2("Testing flip entrypoint")
    scenario.verify(c1.data.side == 0)
    c1.flip()
    scenario.verify(c1.data.side == 1)
    c1.flip()
    scenario.verify(c1.data.side == 0)
    c1.flip()
    scenario.verify(c1.data.side == 1)
```


Test it at https://smartpy.io/ide, then use it as inspiration create your own smart contract :
- call it CountTheCalls
- the storage should contain a value nb_calls, initialized with 0
- increment this value every time this contract is called, through an entrypoint named make_call()
- add a test scenario and run it
- show us your solution in your private channel by using the share link of the smartPy IDE

Here is the cheat-sheet where we include all the syntax elements we use during the training, for SmartPy:

https://docs.google.com/document/d/14PTzLLA2blhI39FbmZ99068CG-dKQrupMsyaArGQ9SI/edit?usp=sharing