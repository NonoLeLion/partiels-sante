Video on addresses in SmartPy: https://youtu.be/Akch_06Y2D4

Modify your endlesswall contract from the previous exercise, so that the same user can't add text twice in a row.

In other words, between two calls from a given user, at least one other user must have added text.

Reminders:
- we introduced a new type sp.address for addresses of users (implicit accounts) and contracts (originated accounts)
- addresses of implicit accounts start with "tz", while those of originated accounts start with "KT"
- to hard-code address, we can use address literals: admin = sp.address("tz1abwc....")
- the address of the direct caller of a contract can be obtained using sp.sender
- we can simply compare addresses: assert sp.sender == admin

In the test scenario:
- we can create new accounts: alice = sp.test_account("Alice")
- then obtain their address: alice.address
- when calling a contract, we can specify who is making the call: c1.add(5).run(sender = alice)
- remember how to check that the contract fails: c1.add(10).run(sender = bob, valid = False)

---

Common issues:
- Smartpy will accept sp.address("tz1") or sp.address("") but this won't work when you deploy. You need to use a real address. My recommendation is to add a parameter to __init__ and send the address of the owner
- make sure you remember that when you do an asset, the condition is the condition that should be true, not the condition that triggers the message
- keep your error messages short.  you have to pay for the storage
- remember that when you call an entrypoint, you shouldn't write scenario += contract.my_entry_point. just contract.my_entry_point
- make sure you do at least 2 successful calls to the entrypoints
- make sure you use the address of users to determine who called last. Don't rely on the name sent as a parameter