Video on transactions in SmartPy: https://youtu.be/Fc501BUfpUc

Change your NFT contract so that the owner can set a price, and other users can purchase it for that price.

You will need two new entrypoints: 
- set_price(newPrice), to let the seller set or update the sale price
- buy(), to be called by the buyer. It will need to:
    - Check that the buyer transferred the correct amount, using sp.amount
    - Send that amount to the seller’s address, from the balance of the contract
    - Transfer ownership of the NFT to the buyer

Reminders:
- In the scenario, when calling a contract, we may send some tez to it: testContract.myEntryPoint.run(sender = alice, amount = sp.tez(5))
- Entrypoints can check the amount transferred: assert sp.amount >= sp.tez(5)
- The amount is immediately added to the balance of the contract
- The entrypoint can check the new balance: assert sp.balance >= sp.tez(10)
- A contract can send some tez to an address: sp.send(userAddress, sp.tez(10))
- Such transactions are not immediate, they are performed after the entrypoint code is executed
- If something fails, everything is canceled:
    - If the balance is insufficient
    - If the destination address is a contract and rejects the transfer
- In the scenario, we can check the contracts balance:
```py
testContract.myEntryPoint.run(sender = alice, amount = sp.tez(5))
balanceAfter = testContract.balance
```

- Mutez are stored as 64 bits unsigned value and have an upper limit. Reaching it causes a failure

---

A solution (with a test scenario that needs to be improved):

```py
import smartpy as sp

@sp.module
def main():

    class NftForSale(sp.Contract):
    
       def __init__(self, owner, metadata, price):
           self.data.owner = owner
           self.data.metadata = metadata
           self.data.price = price
    
       @sp.entrypoint
       def set_price(self, new_price):
           assert sp.sender == self.data.owner, "you cannot update the price"
           self.data.price = new_price
    
       @sp.entrypoint
       def buy(self):
           assert sp.amount == self.data.price, "wrong price"
           sp.send(self.data.owner, self.data.price)
           self.data.owner = sp.sender
    
@sp.add_test()
def test():
    alice = sp.test_account("alice").address
    bob = sp.test_account("bob").address
    eve = sp.test_account("eve").address
    c1 = main.NftForSale(owner = alice, metadata = "My first NFT", price = sp.mutez(5000000))
    scenario = sp.test_scenario("Test", main)
    scenario +=c1
    scenario.h3("Testing set_price entrypoint")
    c1.set_price(sp.mutez(7000000), _sender = alice)
    c1.set_price(sp.tez(5), _sender = bob, _valid = False)

    scenario.h3("Testing buy entrypoint with correct and incorrect prices")
    c1.buy(_sender = bob, _amount = sp.mutez(7000000))
    c1.buy(_sender = eve, _amount=sp.tez(6), _valid = False)
```