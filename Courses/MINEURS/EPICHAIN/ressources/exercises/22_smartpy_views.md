Video on views: https://youtu.be/bNax0-jm37Q  
(currently being processed by youtube, but should be available soon)

![Alt text](img/22_smartpy_views_01.png)
![Alt text](img/22_smartpy_views_02.png)

I'm not requiring that you finish this exercise by next week, but make sure you watch the video.

---

Solution video : https://www.youtube.com/watch?v=PKRoiPlAsms